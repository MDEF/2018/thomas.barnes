---
layout: page
title: My Project
permalink: /My project/
---

**Current direction of project:**

*“To promote a circular approach and increase access to sustainable materials in designing for digital fabrication processes. To use digital fabrication as a model for how other systems can achieve zero waste and promote the message of sustainable design to wider audiences.”*


This page is dedicated to exploring ideas to support this vision. Compiling useful links, images, videos, projects and inspiration.

**My Motivation:**

I've spent the last 10 years in workshops. Working as a designer, model maker and digital fabrication technician. I've made some amazing things, but i'm also very personally aware of the waste i've produced in the process. The end always seems to justify the means, and everyday considerations get in the way of ideal ways of working. When i think of the piles of waste material i've disposed of it horrifies me.

{% figure caption: "Waste in my workshop back in Bristol" %}
![]({{site.baseurl}}/images/waste_in_workshop.png)
{% endfigure %}

**Links:**

Materials:
- **[Materiom](http://materiom.org)**
- **[Material design lab](http://materialdesignlab.dk)**
- **[Materiability](http://materiability.com)**
- **[Materfad](http://es.materfad.com)**

Circular economies and sustainability:
- **[Circular Design Guide](https://www.circulardesignguide.com)**


Creative Commons:
- **[P2P Foundation](https://p2pfoundation.net)**

- Tracking waste
[Trash Track](http://senseable.mit.edu/trashtrack/index.php)

**What are other maker spaces doing?**

I am fortunate to be interested in a subject in which i can get direct access to. Not only in the FabLab in IAAC, but also other maker spaces in Poblenou and Barcelona as a whole. I need to see what other people are doing, and gain information that might support or contradict my own experiences. Here are some places that are will visit.

- [Transfolab](https://www.transfolabbcn.com/home)
- [La Fabrica del Sol](http://ajuntament.barcelona.cat/lafabricadelsol/en)
- [Cannova - Ecolab + Bio Architecture](https://www.cannova.net)
- [Barcelona Wood Workshops](https://www.barcelonawoodworkshops.com/en/)
- [Made BCN Maker Space](http://made-bcn.org)

**Areas to Explore:**

Biofabrication:

Where materials are grown in a lab not in a field

Cosmolocalism:

"Cosmolocalism will document, analyze, test, evaluate, and create awareness about an emerging mode of production, based on the confluence of the digital commons (e.g., open knowledge and design) with local manufacturing and automation technologies"

"Cosmolocalism has three concurrent streams: democratization; innovation; and sustainability.""

<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/MaAOryYIII4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**Related Research:**

- [Making Sustainability: How Fab Labs Address Environmental Sustainability - Cindy Kohtala, March 2016](https://www.researchgate.net/publication/298006534_Making_Sustainability_How_Fab_Labs_Address_Environmental_Sustainability)

**Practical Experiment:**

One of my areas i want to look at is repurposing saw dust. I found it was challenging to repurpose sawdust produced from CNC routers. Particularly if it is made from 'dirty' composite boards. Such as plywood, mdf or chipboard. Wood has already been repurposed once, the process then stops, because the chemicals used are harmful or the dust clogs up filters.

Precious plastics is a good example of how plastic can be shredded, melted down and re-used. Thermoplastics in particular have a chemical make up that support being used repeatedly. However the processes that can be used after currently only include extrusion, moulding and printing. I want to see if materials can be repurposed in a lab that can ultimately be machined.

However firstly i would need to explore just getting a material that can be made from sawdust.

There is a recipe on the materiom website for creating a mouldable material from sawdust.

{% figure caption: "Moulded sawdust pot" %}
![]({{site.baseurl}}/images/sawdust_pot.jpeg)
{% endfigure %}

I ended up doing this activity as part of work in [Living with Ideas week](https://mdef.gitlab.io/thomas.barnes/reflections/living-with-ideas/). It was great to make a start with this idea, and I will continue to experiment with the sawdust based materials.

**Visit to Materfad**

22.11.17

We went to see [MaterFad](http://es.materfad.com), which has an amazing material library, with around 4000 samples of innovative materials from all round the world. They also have an online database with information about all the materials, and where you can get samples from. Nearly all the materials are commercially available.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/materfad_entrance.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/samples.jpg"/>
  </div>
</div>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/Shelves_of_wood.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/materfad_inside.jpg"/>
  </div>
</div>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/wood_exp.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/samples2.jpg"/>
  </div>
</div>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/La_xiloteca.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/materfad/wood_blocks.jpg"/>
  </div>
</div>


**Mapping issues:**

![]({{site.baseurl}}/images/Mapping-issues.svg)
