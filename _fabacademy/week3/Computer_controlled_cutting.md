---
title: Computer Controlled Cutting
period: 7-13 February 2019
date: 2018-2-7 12:00:00
published: true
---

This week we are learning about computer controlled cutting, which includes using laser and vinyl cutters.  

The tasks involved testing the laser cutters, to check their settings and understand how they work.

![]({{site.baseurl}}/speedy100.jpg)

Key steps:

For Machine

- Turn on machine using key
- Wait for bed to move to bottom of machine
- Bring bed back up to correct height for material
- Load material into machine, make sure it is flat, if not use tape or weights if possible (if weights be careful though to keep out of path of laser)
- Focus laser head on material using focus tool
- Place the laser head in the origin point
- Turn on extraction
- Check settings
- Send file to machine

For the files

- Import into Rhino
- Change colours for cutting or raster on separate layers
- Press print
- Check machine is selected, and vector or raster.
- Check the art board fits the machine and parts
- Go to material settings and check parameters for the material and your desired effects if engraving.
- Send to machine.

**Group Assignment**

Press fit joint test

A press-fit joint (also known as interference fit or friction fit) is a fastening between two parts which is achieved by friction after the parts are pushed together, rather than by any other means of fastening.

![]({{site.baseurl}}/pressfittest1.png)

We found a piece of 6mm cardboard. We prepared the rhino file for the test with slots from 5.5mm to 6.5mm.

 ![]({{site.baseurl}}/pressfittest2.jpg)

 ![]({{site.baseurl}}/pressfittest3.jpg)

After printing the two parts we tested all the slots.

After printing the two parts we tested all the slots and our conclusions are:

From 5.5mm to 6mm the joint is too tight and deforms the slot.
6.1mm is perfect, not too tight and not too loose.
And from 6.2mm to 6.5mm it gets worst.

Kerf test

The kerf test is to check how much material is removed as the laser passes through a material. The gap caused by the laser is called the kerf. The thicker the material, the more slowly you have to pass through the material, and the wider the gap will be.

![]({{site.baseurl}}/kerf.png)

In this test we put 9 pieces of wood next to each other with 10 lines. Each piece is 10mm wide. Once the parts were cut the pieces are put together and measured.

- The total should be 90mm
- The parts together measured 89.91mm
- The gap is 0.9mm, and if you divide it by the number of lines which is 10 it gives you kerf width of **0.09mm** with this material and settings.

<br>

- Material - 4mm plywood
- Cutting power - 90
- Cutting speed - 1
- Cutting Hz - 1000
- Machine - Trotec Speedy400


![]({{site.baseurl}}/kerftestmeasure1.jpg)

![]({{site.baseurl}}/kerftestmeasure2.jpg)

Checking the kerf means that you can take into account the size when creating joints and make sure they will fit with the tolerance that you need.


**Individual Assignment**

Creating a Press Fit Kit using the laser cutter


![]({{site.baseurl}}/cccweek1.png)

I first created a shape in Fusion 360 that slots together

![]({{site.baseurl}}/cccweek2.png)

Sketch of part

![]({{site.baseurl}}/cccweek3.png)

Part patterned so they fit together

![]({{site.baseurl}}/cccweek4.png)

I also made a 3 pronged part and included it in the kit

![]({{site.baseurl}}/cccweek5.png)

Using parameters to make the model parametric

![]({{site.baseurl}}/cccweek6.png)

![]({{site.baseurl}}/cccweek7.png)

I didn't constrain the slots properly so they were off centre when i changed the parameters.

![]({{site.baseurl}}/cccweek9.png)

![]({{site.baseurl}}/cccweek10.png)

Constraining the slot so it's always in the middle

![]({{site.baseurl}}/cccweek11.png)

Adding chamfers to improve the fit, and radii to the parts for aesthetic reasons.

![]({{site.baseurl}}/cccweek12.jpg)

Parts put in rhino before laser cutting

![]({{site.baseurl}}/cccweek13.jpg)

Test parts on the laser cutter

![]({{site.baseurl}}/cccweek14.jpg)

Parts copied across the sheet of cardboard

![]({{site.baseurl}}/cccweek15.jpg)

Parts assembled

![]({{site.baseurl}}/cccweek16.jpg)

Download:
[Press Fit Files]({{site.baseurl}}/resources/press-fit.dxf)

**Vinyl Cutting**

![]({{site.baseurl}}/cccweek17.png)

I made a design in illustrator

![]({{site.baseurl}}/cccweek18.jpg)

I used the roland cutter for my test

![]({{site.baseurl}}/cccweek19.jpg)

Checking the width of the part

![]({{site.baseurl}}/cccweek20.jpg)

Test cut on the vinyl cutter to make sure the depth is accurate

![]({{site.baseurl}}/cccweek21.jpg)

![]({{site.baseurl}}/cccweek22.jpg)

Test peice that didn't work

![]({{site.baseurl}}/cccweek23.jpg)

Final piece on my notebook.

Download:
[Vinyl cutter file]({{site.baseurl}}/resources/vinly-cutter-file.ai)
